import React from 'react';
import ElementTemoignage from '../components/ElementTemoignage';

const PageTemoignages = ({ temoignages }) => {
  return (
    <div>
      <h1>Liste des témoignages</h1>
      {temoignages.length === 0 ? (
        <p>Aucun témoignage disponible</p>
      ) : (
        <ul>
          {temoignages.map((temoignage, index) => (
            <ElementTemoignage
              key={index}
              nom={temoignage.nom}
              prenom={temoignage.prenom}
              message={temoignage.message}
              date={temoignage.date}
            />
          ))}
        </ul>
      )}
    </div>
  );
};

export default PageTemoignages;

