import React from 'react';
import FormulaireTemoignage from '../components/FormulaireTemoignage';
import '../components/formulaireTemoignage.css'
const PageFormulaireTemoignage = ({ ajouterTemoignage }) => {
  return (
    <div className='page-formulaire'>
      <h1>Avez vous des commentaires ? Un avis ? Notez ici </h1>
      <FormulaireTemoignage ajouterTemoignage={ajouterTemoignage} />
    </div>
  );
};

export default PageFormulaireTemoignage;


