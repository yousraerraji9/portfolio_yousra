import React from 'react';
import './experiences.css';
import Projet1 from '../components/Projet1.png';
import Projet2 from '../components/Projet2.PNG';
import './projet1Details.css';
import { NavLink } from 'react-router-dom';

const Experiences = () => {
  return (
    <div className="experiences">
      <div className="experience1">
        <img src={Projet1} alt="Capture d'écran" />
        <h2>Best and cheap Electro</h2>
        <h3>Projet de vente des electro en ligne</h3>
        <NavLink to="/projet1-details">
        <button>Aller au détails du Projet 1</button>
        </NavLink>
      </div>
      <div className="experience2">
        <img src={Projet2} alt="Capture d'écran" />
        <h2>Automobile</h2>
        <h3>Projet iOS développé par SwiftUi</h3>
        <NavLink to="/projet2-details">
        <button>Aller au détails du Projet 2</button>
        </NavLink>
      </div>
    </div>
  );
};

export default Experiences;
