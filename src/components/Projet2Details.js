import React from 'react';
//import Projet1CaptureEcran1 from '../components/Projet2CaptureEcran1.png';
import Projet2 from '../components/Projet2.PNG';
import './Projet2Details.css';

const Projet2Details = () => {
  return (
    <div className="projet-details">
      <div className="projet-description">
        <h2>Détails du Projet 1</h2>
        <p>
          Le projet consiste à afficher les differents automobiles de l'entreprise, 
          avec les détails de chaque voiture, 
          le millage, l'année, 
          le type (hybride, rechargeable etc ...)
        </p>
        <h3>Languages utilisés :</h3>
        <ul>
          <li>SwiftUI</li>
        </ul>
      </div>
      <div className="projet2-photos">
        
        <div className="photos-container">
          <img src={Projet2} alt="Capture d'écran 1" />
        </div>
      </div>
      
    </div>
  );
};

export default Projet2Details;