import React from 'react';
import './footer.css'; // Chemin vers le fichier CSS
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLinkedin, faGitlab, faFacebook } from '@fortawesome/free-brands-svg-icons';

const Footer = () => {
  return (
    <div className="container">
      <div className="content">
        {/* Votre contenu de page */}
      </div>
      <footer className="footer">
        <p>
          Développée par Yousra Erraji | &copy; 2023
          <a href="https://www.linkedin.com/in/yousra-erraji-34a9a11b2/" target="_blank" rel="noopener noreferrer">
            <FontAwesomeIcon icon={faLinkedin} />
          </a>
          <a href="https://www.facebook.com/" target="_blank" rel="noopener noreferrer">
            <FontAwesomeIcon icon={faFacebook} />
          </a>
          <a href="https://gitlab.com/votre-projet-gitlab" target="_blank" rel="noopener noreferrer">
            <FontAwesomeIcon icon={faGitlab} />
          </a>
        </p>
      </footer>
    </div>
  );
};

export default Footer;


