import React from 'react';
import './competences.css';

const Competences = () => {
  const competences = [
    { langage: 'C++', pourcentage: 90 },
    { langage: 'C#', pourcentage: 85 },
    { langage: 'Java', pourcentage: 80 },
    { langage: 'JavaScript', pourcentage: 95 },
    { langage: 'HTML', pourcentage: 90 },
    { langage: 'CSS', pourcentage: 85 },
    { langage: 'React', pourcentage: 90 },
    { langage: 'Node.js', pourcentage: 80 },
    { langage: 'Vue.js', pourcentage: 75 },
    { langage: 'SQL Server', pourcentage: 85 },
    { langage: 'MongoDB', pourcentage: 75 },
  ];

  return (
    <div className="competences">
      <h2>Compétences</h2>
      <ul className="competences-liste">
        {competences.map((competence, index) => (
          <li key={index} className="competence-item">
            <span className="competence-langage">{competence.langage}</span>
            <div className="competence-progress">
              <div
                className="competence-progress-bar"
                style={{ width: `${competence.pourcentage}%` }}
              ></div>
            </div>
            <span className="competence-pourcentage">{competence.pourcentage}%</span>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Competences;
