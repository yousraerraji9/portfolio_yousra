import React, { useState } from 'react';
import './contact.css';

const Contact = () => {
  const [formValues, setFormValues] = useState({
    nom: '',
    prenom: '',
    telephone: '',
    email: '',
    message: '',
  });

  const [formErrors, setFormErrors] = useState({
    nom: '',
    prenom: '',
    telephone: '',
    email: '',
    message: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({
      ...formValues,
      [name]: value,
    });
    setFormErrors({
      ...formErrors,
      [name]: '',
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    // Validation des champs
    let errors = {};

    //Valider le nom
    if (formValues.nom.trim().length === 0) {
      errors.nom = 'Le nom est requis';
    } else if (formValues.nom.length > 20) {
      errors.nom = 'Le nom ne peut pas dépasser 20 caractères';
    }

    //valider le prenom
    if (formValues.prenom.trim().length === 0) {
      errors.prenom = 'Le prénom est requis';
    } else if (formValues.prenom.length > 20) {
      errors.prenom = 'Le prénom ne peut pas dépasser 20 caractères';
    }

    //valider le numero de telephone le length et le debut
    if (formValues.telephone.trim().length === 0) {
      errors.telephone = 'Le numéro de téléphone est requis';
    } else if (!/^(\+1)[0-9]{10}$/.test(formValues.telephone)) {
      errors.telephone = 'Le numéro de téléphone doit commencer par le code +1 et contient 10 chiffres';
    }

    //valider l email
    if (formValues.email.trim().length === 0) {
      errors.email = 'L\'adresse e-mail est requise';
    } else if (!/\S+@\S+\.\S+/.test(formValues.email)) {
      errors.email = 'L\'adresse e-mail n\'est pas valide';
    }

    //valider le message
    if (formValues.message.trim().length === 0) {
      errors.message = 'Le message est requis';
    } else if (formValues.message.trim().split(' ').length > 150) {
      errors.message = 'Le message ne peut pas dépasser 150 mots';
    }

    if (Object.keys(errors).length === 0) {
      // Envoyer le formulaire ou effectuer d'autres actions ici
      console.log('Formulaire soumis avec succès', formValues);
      setFormValues({
        nom: '',
        prenom: '',
        telephone: '',
        email: '',
        message: '',
      });
    } else {
      setFormErrors(errors);
    }
  };

  return (
    <div className="contact-container">
      <h2>Contactez-moi</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="nom">Nom:</label>
          <input
            type="text"
            id="nom"
            name="nom"
            value={formValues.nom}
            onChange={handleChange}
          />
          {formErrors.nom && <span className="error-message">{formErrors.nom}</span>}
        </div>

        <div className="form-group">
          <label htmlFor="prenom">Prénom:</label>
          <input
            type="text"
            id="prenom"
            name="prenom"
            value={formValues.prenom}
            onChange={handleChange}
          />
          {formErrors.prenom && <span className="error-message">{formErrors.prenom}</span>}
        </div>

        <div className="form-group">
          <label htmlFor="telephone">Numéro de téléphone:</label>
          <input
            type="text"
            id="telephone"
            name="telephone"
            value={formValues.telephone}
            onChange={handleChange}
          />
          {formErrors.telephone && <span className="error-message">{formErrors.telephone}</span>}
        </div>

        <div className="form-group">
          <label htmlFor="email">Email:</label>
          <input
            type="email"
            id="email"
            name="email"
            value={formValues.email}
            onChange={handleChange}
          />
          {formErrors.email && <span className="error-message">{formErrors.email}</span>}
        </div>

        <div className="form-group">
          <label htmlFor="message">Message:</label>
          <textarea
            id="message"
            name="message"
            value={formValues.message}
            onChange={handleChange}
          />
          {formErrors.message && <span className="error-message">{formErrors.message}</span>}
        </div>

        <button type="submit">Envoyer</button>
      </form>
    </div>
  );
};

export default Contact

