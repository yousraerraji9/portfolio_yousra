import React from 'react';
import Projet1CaptureEcran1 from '../components/Projet1CaptureEcran1.png';
import Projet1CaptureEcran2 from '../components/Projet1CaptureEcran2.png';
import './projet1Details.css';

const Projet1Details = () => {
  return (
    <div className="projet-details">
      <div className="projet-description">
        <h2>Détails du Projet 1</h2>
        <p>
          Le projet consiste à la vente des produits électroniques (téléphones, PC, smartwatches, télévisions, etc.) neufs et
          d'occasion sur une plateforme en ligne accessible sur tous les types de navigateurs et sur mobile. L'idée est d'offrir une
          belle expérience à l'utilisateur en termes de rapport qualité-prix.
        </p>
        <h3>Languages utilisés :</h3>
        <ul>
          <li>Node.js</li>
          <li>React.js</li>
          <li>Material UI</li>
        </ul>
      </div>
      <div className="projet-photos">
        <h3>Captures d'écran :</h3>
        <div className="photos-container">
          <img src={Projet1CaptureEcran1} alt="Capture d'écran 1" />
          <img src={Projet1CaptureEcran2} alt="Capture d'écran 2" />
        </div>
      </div>
    </div>
  );
};

export default Projet1Details;

