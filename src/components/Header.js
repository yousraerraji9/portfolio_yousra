import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import header from './header.css';
import Apropos from './Apropos';
import { faBars, faUser, faUsers, faGraduationCap, faHistory, faCogs, faEnvelope } from '@fortawesome/free-solid-svg-icons';

const Header = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  return (
    <header className="header">
      <div className="logo">
        <NavLink to="/" className="logo-link">
          <span className="logo-initials">Yousra Erraji</span>
        </NavLink>
      </div>
      <nav className="nav">
        <ul className={isOpen ? 'nav-menu active' : 'nav-menu'}>
          <li>
            <FontAwesomeIcon icon={faUser} />
            <NavLink exact to="/apropos" activeClassName="active" onClick={toggleMenu}>
              A propos
            </NavLink>
          </li>
          <li>
            <FontAwesomeIcon icon={faGraduationCap} />
            <NavLink to="/etudes" activeClassName="active" onClick={toggleMenu}>
              Etudes
            </NavLink>
          </li>
          <li>
            <FontAwesomeIcon icon={faHistory} />
            <NavLink to="/experiences" activeClassName="active" onClick={toggleMenu}>
              Expériences
            </NavLink>
          </li>
          <li>
            <FontAwesomeIcon icon={faCogs} />
            <NavLink to="/competences" activeClassName="active" onClick={toggleMenu}>
              Compétences
            </NavLink>
          </li>
          <li>
            <FontAwesomeIcon icon={faUsers} />
            <NavLink exact to="/temoignage" activeClassName="active" onClick={toggleMenu}>
              Témoignages
            </NavLink>
          </li>
          <li>
            <FontAwesomeIcon icon={faEnvelope} />
            <NavLink to="/contact" activeClassName="active" onClick={toggleMenu}>
              Contact
            </NavLink>
          </li>
        </ul>
        
      </nav>
    </header>
  );
};

export default Header;


