import React from 'react';
import './apropos.css'; // Chemin vers le fichier CSS
import yousra from './yousra.jpg'; // Chemin vers l'image du profil

const Apropos = () => {
  return (
    <div className="apropos-container">
      <div className="profile-image">
        <img src={yousra} alt="Profile" />
      </div>
      <div className="apropos-content">
        <h2 className="apropos-title">A propos de moi</h2>
        <p>Je suis une étudiante en programmation informatique passionnée par le développement web et mobile. J'ai acquis une solide expérience dans la création d'applications modernes en utilisant les dernières technologies.</p>
        <p>Mon objectif est de créer des solutions innovantes qui apportent une valeur ajoutée aux utilisateurs. J'ai une forte expertise dans les langages de programmation tels que JavaScript, HTML et CSS, ainsi que dans les frameworks populaires tels que React et Vue.js.</p>
        <p>Je suis constamment en train d'apprendre et de me tenir au courant des nouvelles tendances et des meilleures pratiques dans le domaine du développement.</p>
      </div>
      
    </div>
    
  );
};

export default Apropos;

