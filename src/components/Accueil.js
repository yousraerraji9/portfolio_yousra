import React from 'react';
import { Link } from 'react-router-dom';
import gifImage from './gifImage.gif';
import accueil from './accueil.css';

const Accueil = () => {
  return (
    <div>
            <section className="hero">
        <div className="hero-content">
          <h1>Bienvenue sur mon Portfolio !</h1>
          <p>Je suis Yousra Erraji, étudiante en programmation informatique, passionnée par le développement web et mobile, Cliquez pour décourvir</p>
          <p>I am Yousra Erraji, a student in computer programming, passionate about web and mobile development, Click to discover </p>
          <div className="arrow-down"></div> {/* Ajout de la flèche vers le bas */}
          
          <Link to="/apropos" className="cta-button">En savoir plus</Link>
        </div>
        <div className="gif-image">
          <img src={gifImage} alt="Animation" /> {/* Utilisez la variable gifImage */}
        </div>
      </section>

      {/* Reste du contenu */}
    </div>
  );
};

export default Accueil;
