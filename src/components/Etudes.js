import React, { useState } from 'react';
import './etudes.css'; // Import the CSS file
import logoCite from './logoCite.jpg'
import logo2 from './logo2.png'

const etudes = [
    {
      title: 'Programmation informatique',
      location: 'La cité Collégiale, Ottawa, ON',
      date: 'Graduée en Août 2023',
    },
    {
      title: 'Baccalauréat/Diplome des études secondaires en Sciences Physiques "Option internationale française"',
      location: 'Lycée Moulay Youssef, Rabat, Morocco',
      date: 'Graduée en Juillet 2019',
    },
  ];
  
  const Etudes = () => {
    const [flipped, setFlipped] = useState(false);
    const [showLogo, setShowLogo] = useState(false);
  
    const handleFlip = () => {
      setFlipped(!flipped);
      setShowLogo(true);
    };
  
    return (
      <div className="etudesContainer">
        {etudes.map((etude, index) => (
          <div
            key={index}
            className={`item ${flipped ? 'flipped' : ''}`}
            onClick={handleFlip}
          >
            <div className="card">
            <div className={`front ${flipped ? 'hidden' : ''}`}>
              <div className="front">
                <h2 className="subtitle">{etude.title}</h2>
                <p className="location">{etude.location}</p>
                <p className="date">{etude.date}</p>
                </div>
              </div>
              <div className="back">
                <div className={`logo-container ${showLogo ? 'visible' : ''}`}>
                  {index === 0 && showLogo && (
                    <img
                      src={logoCite}
                      alt="Logo Cite"
                      className="logoCite"
                    />
                  )}
                  {index === 1 && showLogo && (
                    <img
                      src={logo2}
                      alt="Logo 2"
                      className="logo2"
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    );
  };

export default Etudes;
