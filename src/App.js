import './App.css';
import Header from './components/Header';
import { Routes, Route } from 'react-router-dom';
import Footer from './components/Footer';
import Accueil from './components/Accueil';
import Apropos from './components/Apropos';
import Etudes from './components/Etudes';
import Contact from './components/Contact';
import PageFormulaireTemoignage from './pages/PageFormulaireTemoignage';
import PageTemoignages from './pages/PageTemoignages';
//import FormulaireTemoignage from './components/FormulaireTemoignage';
//import TemoignagesListe from './components/TemoignagesListe';
import React, { useState } from 'react';
import Competences from './components/Competences';
import Experiences from './components/Experiences';
import { useHistory } from 'react-router-dom';
import Projet1Details from './components/Projet1Details';

function App() {
  const [temoignages, setTemoignages] = useState([]);

  const ajouterTemoignage = (nouveauTemoignage) => {
    setTemoignages([...temoignages, nouveauTemoignage]);
  };

  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/" element={<Accueil />} />
        <Route path="/apropos" element={<Apropos />} />
        <Route path="/etudes" element={<Etudes />} />
        <Route path="/contact" element={<Contact />} />
        <Route
          path="/temoignage"
          element={<PageFormulaireTemoignage ajouterTemoignage={ajouterTemoignage} />}
        />
        <Route path="/temoignage-List" element={<PageTemoignages temoignages={temoignages} />} />
        <Route path="/competences" element={<Competences />} />
        <Route path="/experiences" element={<Experiences />} />
        <Route path="/projet1-details" element={<Projet1Details />} />

      </Routes>
      <Footer />
    </div>
  );
}

export default App;
